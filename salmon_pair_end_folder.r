library(tximport)
library(readr)
library(optparse)
library(TxDb.Hsapiens.UCSC.hg38.knownGene)
 
option_list <- list(
  make_option(c("-f", "--fastq-dir"), type="character", default="..", 
              help="folder with fatq files [default=%default]", metavar="character"),
	make_option(c("-s", "--salmon-dir"), type="character", default=".", 
              help="the salmon files folder [default=%default]", metavar="character")
); 
 
opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);
#print_help(opt_parser)
#opt$'fastq-dir':
#we know options; we suppose that the index is in salmon forder and it is only one index there
indices <- dir(path=opt$'salmon-dir',pattern="*.index") 
indices <- indices[dir.exists(indices)]
if(length(indices)!=1) {
	stop("Please leave one and only one *.index folder int the salmon dir.",call.=FALSE)
}
index=indices[1]
salmonDir = opt$`salmon-dir`
fastqDir = opt$`fastq-dir`
cat("fastq dir: ",fastqDir,"\n")
cat("salmon dir: ",salmonDir,"\n")
cat("index: ",index,"\n")

fastqfiles=dir(fastqDir,pattern="*gz")
if ( length(grep("_1",fastqfiles))*2==length(fastqfiles) && length(grep("_2",fastqfiles))*2==length(fastqfiles) ) {
	pair<-c("_1","_2")
} else if ( length(grep("_R1",fastqfiles))*2==length(fastqfiles) && length(grep("_R2",fastqfiles))*2==length(fastqfiles) ) {
	pair<-c("_R1","_R2")
} else {
	stop("Can not detect pair end signature. We want to see either _1/_2 or _R1/_R2 classifying all the fastq into two equal groups.",call.=FALSE)
}
#here, we know is it _1 or _R1 naming scheme

samples<-sapply(strsplit(fastqfiles[grep(pair[1],fastqfiles)],split=pair[1],fixed=TRUE),"[[",1)
samples_2<-sapply(strsplit(fastqfiles[grep(pair[2],fastqfiles)],split=pair[2],fixed=TRUE),"[[",1)
if (!setequal(samples,samples_2)) {
	stop("Can not extract sample names.",call.=FALSE)
}

samples=unique(substr(fastqfiles,1,9))
for (sample in samples){
	fastqs<-fastqfiles[grep(sample,fastqfiles)]
	fastq1<-fastqs[grep(pair[1],fastqs)]
	fastq2<-fastqs[grep(pair[2],fastqs)]
	fastq1<-paste0(fastqDir,"/",fastq1)
	fastq2<-paste0(fastqDir,"/",fastq2)
	command<-sprintf("salmon quant -i %s -l IU -1 %s -2 %s --validateMappings -o %s/%s",index,fastq1,fastq2,salmonDir,sample)
	if (!file.exists(paste0(salmonDir,"/",sample,"/","quant.sf"))) {
		cat(command,"\n")
		system(command)
	} else {
		cat(command," : already run\n")
	}
}


#look for quant.sf
sf.files<-dir(path=salmonDir,pattern="*quant.sf",recursive=TRUE)
#we want to know the names of the folders where the files are
#there are the second from the end in the strsplit by "/" 
sf.folders<-sapply(lapply(strsplit(sf.files,split="/",fixed=TRUE),rev),"[[",2)
#this is actually names
#all this is garbege!!!
#we read ano of quant.sf to extract row names and to from tx2gene for tximport
tmpmat=read.table(paste0(salmonDir,"/",sf.files[1]),sep="\t",header=TRUE,row.names=1)

transcript_ids<-rownames(tmpmat)

#we need the full path to salmon file for txiimport 
salmonfiles<-paste0(salmonDir,"/",sf.files)
#and their names are the sample names 
names(salmonfiles)<-sf.folders

txdb <- TxDb.Hsapiens.UCSC.hg38.knownGene
k <- keys(txdb, keytype = "TXNAME")
tx2gene <- select(txdb, k, "GENEID", "TXNAME")

#import salmon results
txi <- tximport(salmonfiles, type = "salmon", tx2gene = tx2gene)
#save!!!
saveRDS(txi,paste0(salmonDir,"/txi.rds"))

